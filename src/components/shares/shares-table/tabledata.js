import React from "react";
import './styles/tabledata.css'

const ShareTable = (props) => (
    <div className="table-container">
        <table className="table">
            <thead className="head">
                <tr className="head-row">
                    <th>Date</th>
                    <th>Name</th>
                    <th>Amount</th>
                    <th>Quantity</th>
                </tr>
            </thead>
            <tbody className="body">
                {React.Children.toArray(
                    props.shares.map((share) => (
                        <tr className="row">
                            <td>{share.date}</td>
                            <td>{share.name}</td>
                            <td>{share.amount}</td>
                            <td>{share.quantity}</td>
                        </tr>
                    ))
                )}
            </tbody>
        </table>
    </div>
);


export default ShareTable;
