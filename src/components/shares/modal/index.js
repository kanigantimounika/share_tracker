import React from 'react';
import DatePicker  from 'react-datepicker'
import "react-datepicker/dist/react-datepicker.css";
import './styles/index.css'

class Modal extends React.Component{

    constructor(props)
    {
        super(props)
        this.state={
            setDate:new Date()
    
        }

    }
    handleChangedDate=(date)=>{
        this.setState({setDate:date})
        

    }
    onFormSubmit=(e)=>{
        e.preventDefault();
        //console.log(e.target.elements.input.value)
        const name=e.target.elements.nametextbox.value
        const amount=e.target.elements.amounttextbox.value;
        const quantity=e.target.elements.quantitytextbox.value;
        const date=this.state.setDate.getFullYear()+'/'+this.state.setDate.getMonth()+'/'+this.state.setDate.getDate();
        e.target.elements.nametextbox.value="";
        e.target.elements.amounttextbox.value="";
        e.target.elements.quantitytextbox.value="";
        this.props.onSubmitHandler(date,name,amount,quantity)

        this.props.close()

    }
    
    render(){
        return (
            <div className="modal-main-container">
                <div className="modal-container">
                    <div className="modal-header">
                        <span className="button-close" onClick={this.props.close}>x</span>  
                    </div>
                    <div className="modal-body">
                        <form onSubmit={this.onFormSubmit}>
                            <div className="name-input">
                                <span className="description">Name</span>
                                <input type="text" className="nametextbox" name="nametextbox" placeholder="Enter name of the stock"></input>
                                
                            </div>
                            <div className="amount-input">
                                <span className="description">Amount</span>
                                <input type="text" className="amounttextbox" name="amounttextbox" placeholder="Enter Price of the stock"></input>
                            </div>
                            <div className="quantity-input">
                                <span className="description">Quantity</span>
                                <input type="number" className="quantitytextbox" name="quantitytextbox"></input>
                            </div>
                            <div className="buying-date">
                                <span className="description">Date</span>
                                <DatePicker 
                                dateFormat="MM/dd/yyyy"
                                selected={this.state.setDate}
                                onChange={this.handleChangedDate}
                                
                                ></DatePicker>
                            </div>
                            <div className="form-submit-btn">
                                <button className="submit-btn" >Submit</button>
                            </div>

                        </form>

                    </div>
                </div>
            </div>
        )

    }
}

 
  

export default Modal
