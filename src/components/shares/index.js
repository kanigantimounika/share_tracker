import React from 'react';
import './styles/index.css'
import Modal from './modal/index';

import SharesTable from './shares-table/tabledata'


class HomePage extends React.Component{
    constructor(props)
    {
        super(props);
        this.state = {
            isShowing: false,
            shares:[
                {date:"28/09/2021",
                name:"zeel",
                amount:215,
                quantity:10
                }

            ]

        }
    }
    openModalHandler = () => {
        this.setState({
          isShowing: true
        });
      }
    
    closeModalHandler = () => {
        this.setState({
          isShowing: false
        });
      }

    getButtonContainer=()=>{
        return(
            
                <button className="open-modal-btn" onClick={this.openModalHandler}>Add Share</button> 
           
        )
    }

    onSubmitHandler=(date,name,amount,quantity)=>{
          
        this.setState((prev)=>(

            {...prev,shares:[...prev.shares,{date:date,name:name,amount:amount,quantity:quantity}]}
            ))   

    }
    render(){

        
        return(
            <div className="component-container">
                <div className="header-container">
                   
                    {this.getButtonContainer()}
                </div>
                <SharesTable shares={this.state.shares}/>

                    
                
                { this.state.isShowing ? 
                    <Modal
                    className="modal"
                    show={this.state.isShowing}
                    close={this.closeModalHandler}
                    onSubmitHandler={this.onSubmitHandler}>
                    </Modal> : null 
                }

            </div>
    

        )
    }
}



export default HomePage; 
